package xml;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import xml.products.TownList;

public class CreateNewXML {
  
  public static void create(){
    
    
    ProductSystemBuilder system = new ProductSystemBuilder();
    system.build();
    

    try (FileOutputStream output = new FileOutputStream("Rezult.xml")) {
      
      JAXBContext jaxbContext = JAXBContext.newInstance(TownList.class);

      Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

      jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
      jaxbMarshaller.marshal(system.towns, output);
      jaxbMarshaller.marshal(system.towns, System.out);

    } catch (JAXBException e) {
      System.out.println("TRY HARD");
      e.printStackTrace();
    } catch (FileNotFoundException e) {
    } catch (IOException e) {
    }
      
  }
    
}
