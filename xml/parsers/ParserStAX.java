package xml.parsers;

import java.io.File;

import javax.xml.XMLConstants;
import javax.xml.bind.*;
import javax.xml.stream.*;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.xml.sax.SAXException;

import xml.products.*;

public class ParserStAX {

  public static void parse(String source) {

    try {

      XMLInputFactory xif = XMLInputFactory.newFactory();
      StreamSource xml = new StreamSource(source);
      XMLStreamReader xsr = xif.createXMLStreamReader(xml);
      
      SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI); 
      Schema schema = sf.newSchema(new File("NewXMLSchema1.xsd"));
      
      
      xsr.nextTag();
      String open = xsr.getLocalName();
      while (!xsr.getLocalName().equals("/" + open)) {
      
        while (!xsr.getLocalName().equals("town")) {
          xsr.nextTag();
        }

        JAXBContext jc = JAXBContext.newInstance(Town.class);
        Unmarshaller unmarshaller = jc.createUnmarshaller();
        
        unmarshaller.setSchema(schema);
        unmarshaller.setEventHandler(new MyValidationEventHandler());
        
        
        JAXBElement<Town> jb = unmarshaller.unmarshal(xsr, Town.class);
        xsr.close();

        Town town = jb.getValue();
        
        ModelsAndPrice.forTown(town);
        


        
        
        xsr.nextTag();
      }

    } catch (JAXBException e) {
    } catch (XMLStreamException e) {
    } catch (SAXException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }
}

