package xml.parsers;

import xml.products.Town;

public class ModelsAndPrice {

  public static void forTown(Town town) {

    System.out.print(town.getName() + " ");

    if (town.getProductsInTown() != null) {
      System.out.print(town.getProductsInTown().getProductModels() + " Общая стоимость: ");
      System.out.println(town.getProductsInTown().getProductsPrice());
      System.out.println(town.getProductsInTown().getProductDates());
    } else {
      System.out.println("продукты в город не завезены");
    }

  }
}
