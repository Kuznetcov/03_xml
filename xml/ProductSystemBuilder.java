package xml;


import java.util.List;
import java.util.ArrayList;

import xml.products.*;

public class ProductSystemBuilder {

  TownsType townType;
  List<Town> townWithProducts = new ArrayList<>();
  List<Town> townWithoutProducts = new ArrayList<>();
  TownList towns;

  public void build() {

    for (int i = 1; i < 4; i++) { // Генерация города с продуктами

      ProductList products = new ProductList();
      for (int j = 1; j < 4; j++) { // Генерация продуктов в конкретном городе
        products.addProduct(new Product("Apple", "ee140", "10-10-2000", "RED", 322, 15));
      }
      this.townWithProducts.add(new Town(products, "AIRES"));
    }
    
    for (int i = 1; i < 10; i++) { // Генерация городов без продуктов
      this.townWithoutProducts.add(new Town("ALABAMA"));
    }
    
    this.towns = new TownList();
    towns.addTownList(new TownsType("Города с продуктами",this.townWithProducts));
    towns.addTownList(new TownsType("Города без продуктов",this.townWithoutProducts));
    
  }
}
