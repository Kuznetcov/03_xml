package xml.products;


import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class TownList {

//  public TownList(List<Town> townWithProduct, List<Town> townWithoutProduct) {
//    this.townWithProduct = townWithProduct;
//    this.townWithoutProduct = townWithoutProduct;
//  }
  @XmlElement
  List<TownsType> towns = new ArrayList<>();
  
  public void addTownList (TownsType townList){
    towns.add(townList);   
  }
  
}
