package xml.products;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(name="Name")
public class Town {
  
  public Town(){
  }
  public Town(String name) {
    this.name = name;
  }
  public Town(ProductList productsInTown, String name) {
    this.productsInTown = productsInTown;
    this.name = name;
  }
  
  
  ProductList productsInTown;
  public ProductList getProductsInTown() {
    return productsInTown;
  }
  @XmlElement
  public void setProductsInTown(ProductList productsInTown) {
    this.productsInTown = productsInTown;
  }

  
  String name;
  public String getName() {
    return name;
  }
  @XmlElement
  public void setName(String name) {
    this.name = name;
  }
  
}
