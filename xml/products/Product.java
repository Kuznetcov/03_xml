package xml.products;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;


public class Product {
  
  public Product(){
  }
  
  public Product(String producer, String model, String date, String color, int price,
      int count) {
    this.producer = producer;
    this.model = model;
    this.setDate(date);
    this.color = Colors.valueOf(color);
    this.price = price;
    this.count = count;
  }

  enum Colors {
    RED, BLUE, BROWN, BLACK, WHITE, ORANGE, YELLOW, GREEN, PURPLE
  }

  
  private String producer;
  private String model;
  private LocalDate date;
  private Colors color;
  private Integer price;
  private Integer count;


  public String getProducer() {
    return producer;
  }

  public void setProducer(String producer) {
    this.producer = producer;
  }

  public String getModel() {
    return model;
  }

  public void setModel(String model) {
    this.model = model;
  }

  public String getDate() {
    return date.format(DateTimeFormatter.ofPattern("dd-MM-yyyy"));
  }
  
//не используется потому что XML не дружит с LocalDate
//  public LocalDate getDate() {
//    return date;
//  }  
  
  public void setDate(String date) {
    this.date = LocalDate.parse(date,DateTimeFormatter.ofPattern("dd-MM-yyyy"));
  }

  public Colors getColor() {
    return color;
  }

  public void setColor(Colors color) {
    this.color = color;
  }

  public Integer getPrice() {
    return price;
  }

  public void setPrice(Integer price) {
    this.price = price;
  }

  public Integer getCount() {
    return count;
  }

  public void setCount(Integer count) {
    this.count = count;
  }

}
