package xml.products;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

public class TownsType {

  public TownsType(String typeName, List<Town> town) {
    
    this.typeName = typeName;
    this.town = town;
  }
  @XmlAttribute
  String typeName;
  List<Town> town = new ArrayList<>();

  @XmlElement
  public void setTown(List<Town> townWithProduct) {
    this.town = townWithProduct;
  }

  public List<Town> getTown() {
    return town;
  }
  
  
}
