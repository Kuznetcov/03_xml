package xml.products;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.xml.bind.annotation.XmlElement;

public class ProductList {
  @XmlElement
  List<Product> prod = new ArrayList<Product> ();

  public List<Product> getProd() {
    return prod;
  }
  
  public String getProductModels(){
    
    return prod.stream().map(p -> p.getModel()).collect(Collectors.joining(" "));
  }
  
  public Integer getProductsPrice(){

    return prod.stream().mapToInt((s) -> s.getPrice()).sum();
  }
  
  public String getProductDates(){

    return prod.stream().map(p -> p.getDate()).collect(Collectors.joining(" "));
  
  }
  
//  public void setProd(List<Product> prod) {
//    this.prod = prod;
//  }

  public void addProduct (Product product){
    prod.add(product);   
  }
  
}
